package com.example.dagger2basedapp.di

import com.bumptech.glide.Glide
import com.bumptech.glide.RequestManager
import com.bumptech.glide.request.RequestOptions
import com.example.dagger2basedapp.app.BaseApplication
import com.example.dagger2basedapp.data.network.ApiService
import com.example.dagger2basedapp.data.repo.RepositoryImpl
import com.example.dagger2basedapp.domain.repo.Repository
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

@Module
class AppModule {

    companion object {

        /**
         * application==null returns false,
         * cause @Binds.Instance contributes to have
         * application class object in every Module
         */

        @Provides
        fun getApp(application: BaseApplication): Boolean {
            return application == null
        }

        @Provides
        fun provideAPiService(retrofit: Retrofit): ApiService {
            return retrofit.create(ApiService::class.java)
        }

        @Provides
        fun provideRetrofitInstance(): Retrofit {
            return Retrofit.Builder()
                .baseUrl("http://online.cloud.com.ge/api/")
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
        }

        @Provides
        fun provideGlideInstance(
            application: BaseApplication,
            requestOptions: RequestOptions
        ): RequestManager {
            return Glide.with(application).setDefaultRequestOptions(requestOptions)
        }

        @Provides
        fun provideRepository(apiService: ApiService): Repository {
            return RepositoryImpl(apiService)
        }


    }
}