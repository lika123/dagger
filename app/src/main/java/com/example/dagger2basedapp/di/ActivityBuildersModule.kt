package com.example.dagger2basedapp.di

import com.example.dagger2basedapp.presentation.CurrencyActivity
import dagger.Module
import dagger.Provides
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuildersModule {

    @ContributesAndroidInjector
    abstract fun contributeAuthActivity(): CurrencyActivity

    companion object{

        @Provides
        fun getCurrency(): String {
            return "dsbfjsdbfsdbf"
        }
    }

}