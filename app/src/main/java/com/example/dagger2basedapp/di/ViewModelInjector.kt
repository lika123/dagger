package com.example.dagger2basedapp.di

import com.example.dagger2basedapp.presentation.CurrencyListViewModel
import com.example.dagger2basedapp.presentation.auth.RegisterViewModel
import dagger.Component

@Component(modules = [AppModule::class])
interface ViewModelInjector {

    fun inject(currencyListViewModel: CurrencyListViewModel)
    fun inject(registerViewModel: RegisterViewModel)

    @Component.Builder
    interface Builder {

        fun build(): ViewModelInjector

    }
}