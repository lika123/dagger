package com.example.dagger2basedapp.data.repo

import com.example.dagger2basedapp.domain.model.Currency
import com.example.dagger2basedapp.data.network.ApiService
import com.example.dagger2basedapp.domain.model.Register
import com.example.dagger2basedapp.domain.repo.Repository
import io.reactivex.Completable
import io.reactivex.Observable
import javax.inject.Inject

class RepositoryImpl @Inject constructor(private val apiService: ApiService) :
    Repository {

    override fun getCurrency(): Observable<List<Currency>> =
        apiService.getCurrencyTest("CurrencyRates")

    override fun registerUser(userRegister: Register): Completable =
        apiService.registerUser(userRegister)
}