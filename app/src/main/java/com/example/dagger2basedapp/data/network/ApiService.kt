package com.example.dagger2basedapp.data.network

import com.example.dagger2basedapp.domain.model.Currency
import com.example.dagger2basedapp.domain.model.Register
import io.reactivex.Completable
import io.reactivex.Observable
import retrofit2.http.*

interface ApiService {
    @GET("rates/{path}")
    fun getCurrencyTest(@Path("path") currency:String?) : Observable<List<Currency>>

    @POST("register")
    fun registerUser(@Body user:Register):Completable

}