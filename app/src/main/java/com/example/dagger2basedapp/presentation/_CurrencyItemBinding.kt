package com.example.dagger2basedapp.presentation

import com.example.dagger2basedapp.databinding.CurrencyItemBinding
import com.example.dagger2basedapp.domain.model.Currency


fun CurrencyItemBinding.bindWith(currency: Currency) {
    currencyName.text = currency.currency
    currencyName.text = currency.currencyName
    buyRate.text = currency.buyRate.toString()
    sellRate.text = currency.sellRate.toString()
    nbgRate.text = currency.nbgRate.toString()
    currencyCountryFlag.glide(currency.currencyCountryFlag)
}