package com.example.dagger2basedapp.presentation.auth

import androidx.lifecycle.ViewModel
import com.example.dagger2basedapp.domain.usecase.RegisterUserUseCase
import com.example.dagger2basedapp.presentation.BaseViewModel
import javax.inject.Inject

class RegisterViewModel @Inject constructor(registerUserUseCase: RegisterUserUseCase) : BaseViewModel() {

}