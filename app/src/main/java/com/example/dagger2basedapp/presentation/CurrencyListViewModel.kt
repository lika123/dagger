package com.example.dagger2basedapp.presentation

import androidx.lifecycle.MutableLiveData
import com.example.dagger2basedapp.domain.model.Currency
import com.example.dagger2basedapp.domain.usecase.GetCurrencyUseCase
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import javax.inject.Inject

class CurrencyListViewModel @Inject constructor(getCharactersUseCase: GetCurrencyUseCase) :
    BaseViewModel() {

    private val error = MutableLiveData<String>()
    val currency = MutableLiveData<List<Currency>>()

    var bag = CompositeDisposable()

    init {
        getCharactersUseCase.invoke().subscribe({ currencyList ->
            currency.postValue(currencyList)
        }, { onError ->
            error.postValue(onError.toString())
        }).disposedBy()

    }

    private fun Disposable.disposedBy() {
        bag.add(this)
    }

    override fun onCleared() {
        super.onCleared()
        bag.clear()
    }

}