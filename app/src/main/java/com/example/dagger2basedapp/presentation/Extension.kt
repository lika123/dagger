package com.example.dagger2basedapp.presentation

import android.widget.ImageView
import com.bumptech.glide.Glide

fun ImageView.glide(uri: String?) {

    Glide.with(context).load(uri).centerCrop()
        .into(this)
}
