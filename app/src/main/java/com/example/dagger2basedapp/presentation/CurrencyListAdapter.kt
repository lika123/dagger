package com.example.dagger2basedapp.presentation

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.dagger2basedapp.databinding.CurrencyItemBinding
import com.example.dagger2basedapp.domain.model.Currency

class CurrencyListAdapter: RecyclerView.Adapter<CurrencyListAdapter.ViewHolder>() {

    var characters = mutableListOf<Currency>()

    inner class ViewHolder(binding: CurrencyItemBinding):RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =  ViewHolder(CurrencyItemBinding.inflate(
        LayoutInflater.from(parent.context), parent, false))

    override fun getItemCount(): Int = characters.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        CurrencyItemBinding.bind(holder.itemView).bindWith(characters[position])
    }

    fun updateData(currency:Collection<Currency>){
        this.characters.clear()
        this.characters.addAll(currency)
        notifyDataSetChanged()
    }
}