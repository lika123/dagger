package com.example.dagger2basedapp.presentation

import androidx.lifecycle.ViewModel
import com.example.dagger2basedapp.di.AppModule
import com.example.dagger2basedapp.di.DaggerViewModelInjector
import com.example.dagger2basedapp.di.ViewModelInjector
import com.example.dagger2basedapp.presentation.auth.RegisterViewModel

abstract class BaseViewModel : ViewModel() {
    private val injector: ViewModelInjector =
        DaggerViewModelInjector.builder().build()

    init {
        inject()
    }

    private fun inject(){
        when(this){
            is CurrencyListViewModel -> injector.inject(this)
            is RegisterViewModel -> injector.inject(this)
        }
    }

}