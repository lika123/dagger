package com.example.dagger2basedapp.presentation

import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.dagger2basedapp.databinding.CurrencyActivityBinding
import dagger.android.support.DaggerAppCompatActivity
import javax.inject.Inject

class CurrencyActivity : DaggerAppCompatActivity() {
    private val characterAdapter = CurrencyListAdapter()

    @Inject lateinit var viewModel:CurrencyListViewModel

    @Inject lateinit var string:String

    private lateinit var binding:CurrencyActivityBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = CurrencyActivityBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        binding.charactersRecyclerView.adapter = characterAdapter
        binding.charactersRecyclerView.layoutManager = LinearLayoutManager(this)
        string
        setLog()
    }

    private fun setLog(){
        viewModel.currency.observe(this, Observer{
            characterAdapter.updateData(it)
        })
    }
}