package com.example.dagger2basedapp.domain.model

import com.google.gson.annotations.SerializedName

class Register(
    val name: String,
    val lastName: String,
    val email: String,
    @SerializedName("birth_date") val birthDate: String,
    @SerializedName("phone_number ") val phoneNumber: String,
    @SerializedName("personal_number ") val personalNumber: String,
    val address: String,
    val password: String,
    @SerializedName("password_confirmation ") val passwordConfirmation: String
)