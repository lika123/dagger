package com.example.dagger2basedapp.domain.model

import javax.inject.Inject


data class Currency @Inject constructor(
    val currency: String,
    val currencyName: String,
    val buyRate: Float,
    val sellRate: Float,
    val nbgRate: Float,
    val currencyCountryFlag: String
)
