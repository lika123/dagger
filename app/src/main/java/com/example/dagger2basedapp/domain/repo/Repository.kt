package com.example.dagger2basedapp.domain.repo

import com.example.dagger2basedapp.domain.model.Currency
import com.example.dagger2basedapp.domain.model.Register
import io.reactivex.Completable
import io.reactivex.Observable

interface Repository {
    fun getCurrency(): Observable<List<Currency>>
    fun registerUser(userRegister: Register): Completable
}