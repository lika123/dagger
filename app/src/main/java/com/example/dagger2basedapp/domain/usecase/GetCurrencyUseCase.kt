package com.example.dagger2basedapp.domain.usecase

import com.example.dagger2basedapp.domain.model.Currency
import com.example.dagger2basedapp.domain.repo.Repository
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class GetCurrencyUseCase @Inject constructor(private val repository: Repository) {

    operator fun invoke(): Observable<List<Currency>> =
        repository.getCurrency().subscribeOn(Schedulers.io())
            .subscribeOn(AndroidSchedulers.mainThread())
}