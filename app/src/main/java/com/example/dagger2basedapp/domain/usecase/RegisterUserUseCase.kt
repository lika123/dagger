package com.example.dagger2basedapp.domain.usecase

import com.example.dagger2basedapp.domain.model.Register
import com.example.dagger2basedapp.domain.repo.Repository
import io.reactivex.Completable
import javax.inject.Inject

class RegisterUserUseCase @Inject constructor(private val repository: Repository) {

     fun registerUser(registerUser: Register): Completable {
        return repository.registerUser(registerUser)
    }


}